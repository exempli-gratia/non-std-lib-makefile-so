# some sample Makefile

MAJOR := 1
MINOR := 2
MICRO := 3

NAME := non-std-foo
VERSION := $(MAJOR).$(MINOR).$(MICRO)

soname=lib$(NAME).so.$(MAJOR)
libname=$(soname).$(MINOR).$(MICRO)

lib: lib$(NAME).so.$(VERSION)

all_targets += $(libname)

all: $(all_targets)

lib$(NAME).so: lib$(NAME).so.$(VERSION)
	ldconfig -v -n .
	ln -s lib$(NAME).so.$(MAJOR) lib$(NAME).so

lib$(NAME).so.$(VERSION): $(NAME).o
	$(CC) $(CFLAGS) $(LDFLAGS) -shared -Wl,-soname,lib$(NAME).so.$(MAJOR) $^ -o $@

install: $(all_targets)
	# the header file
	install -D -m 644 $(NAME).h $(includedir)/$(NAME).h
	# the rest
	install -D -m 755 $(libname) $(libdir)/$(libname)
	ln -sf $(libname) $(libdir)/$(soname)
	ln -sf $(libname) $(libdir)/lib$(NAME).so

clean:
	$(RM) $(all_targets) *.o *.so*
