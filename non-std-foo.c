/* this is my libfoo */

/* only here it's actually defined a not extern */
#define LIB_FOO_PRIVATE_PROTOTYPES
#include "non-std-foo.h"
#undef LIB_FOO_PRIVATE_PROTOTYPES

void hello_from_foolib() {
  printf("Hello from non std foolib!\n");
}
