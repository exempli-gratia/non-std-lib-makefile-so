#!/bin/bash
# clean
./02_clean.sh

# build
make

# install
set -x
mkdir -p include/non-std lib/non-std 
includedir=include/non-std libdir=lib/non-std make install
set +x

tree

