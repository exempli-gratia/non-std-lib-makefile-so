# clean
./02_clean.sh

# build
make

# install
mkdir -p include/non-std lib/non-std
includedir=include/non-std libdir=lib/non-std make install

├── 01_build.sh
├── 02_clean.sh
├── include
│   └── non-std
│       └── non-std-foo.h
├── lib
│   └── non-std
│       ├── libnon-std-foo.so -> libnon-std-foo.so.1.2.3
│       ├── libnon-std-foo.so.1 -> libnon-std-foo.so.1.2.3
│       └── libnon-std-foo.so.1.2.3
├── libnon-std-foo.so.1.2.3
├── Makefile
├── non-std-foo.c
├── non-std-foo.h
├── non-std-foo.o
├── readme.txt


